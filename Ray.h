#ifndef _RAY_H
#define _RAY_H

#include "Vect.h"
class Ray {
	//Rays are made of vectors.
	Vect origin, direction; 

	public:

	Ray();

	Ray (Vect, Vect);

// methods
	Vect getRayOrigin(){return origin;}
	Vect getRayDirection(){return direction;}

};

Ray::Ray(){
	origin = Vect(0,0,0);
	// X direction is the default
	direction = Vect(1,0,0);
}

Ray::Ray(Vect o, Vect d){
	origin = o;
	direction = d;
}

#endif