#ifndef _SPHARE_H
#define _SPHARE_H

#include "math.h"
#include "Object.h"
#include "Vect.h"
#include "Color.h"

class Sphare : public Object{
	//Rays are made of vectors.
	Vect center;
	double radius;
	Color color;  

	public:

	Sphare();

	Sphare (Vect, double, Color);

// methods
	Vect getSphareCenter(){return center;}
	double getSphareRadius(){return radius;}
	Color getSphareColor(){return color;}

	Vect getNormalAt(Vect point){
			// normal alway points away from the center
		Vect normal_vect  =point.vectAdd(center.negative()).normalize();
		return normal_vect;
	}
	// it will return the distance between the ray and intersection
	double findIntersection(Ray ray){

		Vect ray_origin = ray.getRayOrigin();
		double ray_origin_x = ray_origin.getVectX();
		double ray_origin_y = ray_origin.getVectY();
		double ray_origin_z = ray_origin.getVectZ();

		Vect ray_direction = ray.getRayOrigin();
		double ray_direction_x = ray_direction.getVectX();
		double ray_direction_y = ray_direction.getVectY();
		double ray_direction_z = ray_direction.getVectZ();

		Vect sphare_center = center;
		double sphare_center_x = sphare_center.getVectX();
		double sphare_center_y = sphare_center.getVectY();
		double sphare_center_z = sphare_center.getVectZ();

		double a = 1; //normalized
		double b = (2*(ray_origin_x-sphare_center_x)*ray_direction_x) + (2*(ray_origin_y-sphare_center_y)*ray_direction_y) + (2*(ray_origin_z-sphare_center_z)*ray_direction_z);
		double c = pow(ray_origin_x - sphare_center_x, 2) + pow(ray_origin_y - sphare_center_y, 2) + pow(ray_origin_z - sphare_center_z, 2) - (radius*radius);

		double discriminant = b*b - 4*c; 

		if (discriminant > 0){
			// the ray intersect sphare

// the first root
			double root_1 = ((-1*b - sqrt(discriminant))/2) - 0.000001;

			if (root_1 > 0) {
				// the first root is the smallest postivice root
				return root_1;
			}
			else{
				// the second root is the smallest postivie root
				double root_2 = (sqrt(discriminant) - b/2) - 0.000001;
				return root_2;
			}


		}
		else {
			// the ray missed the sphare
			return -1; 
		}

	}

};

Sphare::Sphare(){
	center = Vect(0,0,0);
	radius = 1.0;
	color  = Color(0.5, 0.5, 0.5, 0);
}

Sphare::Sphare(Vect centerValue, double	radiusValue, Color colorValue){
	center = centerValue;
	radius = radiusValue;
	color  = colorValue;
}

#endif